// This is required due to a bug in dependencies
require("es6-promise").polyfill();

var path = require("path");
var webpack = require("webpack");

// We create a generator function for our config so that not only does this
// file export the configuration itself, it also exports a generator for itself so 
// that you can easily create derived configurations without having to clone objects.
// Just use requre("./webpack.config.js").generate() to get a new instance of the config object.
var generator = function () {

    // webpack config object
    return {
        cache: true,
        devtool: "inline-source-map",
        entry: [
            "./wwwroot/js/base/site.tsx"
        ],
        output: {
            path: path.resolve("./wwwroot/js"),
            publicPath: "js/",
            filename: "bundle.js",
            sourceMapFilename: "[file].map"
        },
        module: {
            loaders: [
                {
                    test: /\.js(x?)$/,
                    loader: "babel-loader?presets[]=es2015&presets[]=react",
                    exclude: /node_modules/
                },
                {
                    test: /\.ts(x?)$/,
                    loader: "babel-loader?presets[]=es2015&presets[]=react!ts-loader",
                    exclude: /node_modules/
                },
                {
                    test: /\.scss?$/,
                    loaders: ["style", "css?sourceMap", "sass?sourceMap"]
                }
            ]
        },
        plugins: [
            new webpack.ProvidePlugin({
                React: "react",
                ReactDOM: "react-dom"
            })
        ],
        resolve: {
            extensions: ["", ".webpack.js", ".web.js", ".ts", ".tsx", ".js", ".jsx"]
        }
    };
};

// Create a new config for exporting
var initial = generator();
// But attach our generator to it first for others to use.. anti pattern? 
initial.generator = generator;

module.exports = initial;