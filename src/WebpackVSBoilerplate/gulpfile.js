﻿var webpack = require("webpack");
var webpackstream = require("webpack-stream");
var runSequence = require("run-sequence");
var gulp = require("gulp");
var rimraf = require("rimraf");

var paths = {
    jsEntry: "./wwwroot/js/base/site.tsx",
    webEntry: "./wwwroot/index.html",
    dist: "../../artifacts/dist/",
    distJs: "../../artifacts/dist/js",
    webpackConfig: "./webpack.prod.config.js"
};

// dist:clean task
// Deletes dist folder
gulp.task("dist:clean", function (cb) {
    rimraf(paths.dist, cb);
});

// dist:webpack task
// Runs webpack using production config and outputs to dist folder
gulp.task("dist:webpack", function () {
    return gulp.src(paths.jsEntry)
               .pipe(webpackstream(require(paths.webpackConfig)))
               .pipe(gulp.dest(paths.distJs));
});

// dist:copy task
// Copies index.html to dist folder
gulp.task("dist:copy", function () {
    return gulp.src(paths.webEntry, { base: "wwwroot" })
               .pipe(gulp.dest(paths.dist));
});

// dist task
// Run dist:clean first then dist:webpack and dist:copy in parallel
gulp.task("dist", function (cb) {
    runSequence(
        "dist:clean",
        ["dist:webpack", "dist:copy"],
        cb);
});
