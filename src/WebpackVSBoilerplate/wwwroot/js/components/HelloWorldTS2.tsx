﻿/// <reference path="../../../typings/browser.d.ts" />
import * as React from "react";

class HelloWorldTS2 extends React.Component<{}, {}> {
    public render() {
        return (
          <div className="helloWorld">
                <span>Hello, world! .. TS2!</span>
          </div>
      );
    }
}

export default HelloWorldTS2;