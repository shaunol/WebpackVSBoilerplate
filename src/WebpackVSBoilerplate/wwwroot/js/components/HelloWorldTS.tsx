﻿/// <reference path="../../../typings/browser.d.ts" />
import * as React from "react";
var HelloWorldJS = require("./HelloWorldJS.jsx") as any;

class HelloWorldTS extends React.Component<{}, {}> {
    public render() {
        return (
          <div className="helloWorld">
                <span>Hello, world!</span>
                <div>
                    <HelloWorldJS />
                </div>
          </div>
      );
    }
}

export default HelloWorldTS;