﻿var React = require("react");
var HelloWorldTS2 = require("./HelloWorldTS2.tsx").default;

var HelloWorldJS = React.createClass({
    render: function () {
        return (
            <div className="helloWorld">
                <span>Hello, world! .. JS!</span>
                <div>
                    <HelloWorldTS2 />
                </div>
            </div>
        );
    }
});

module.exports = HelloWorldJS;