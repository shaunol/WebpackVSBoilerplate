﻿/// <reference path="../../../typings/browser.d.ts" />
import * as ReactDOM from "react-dom";

import HelloWorldTS from "../components/HelloWorldTS";
require("../../scss/components/HelloWorld.scss");

ReactDOM.render(
    <HelloWorldTS /> ,
    document.getElementById("content")
);