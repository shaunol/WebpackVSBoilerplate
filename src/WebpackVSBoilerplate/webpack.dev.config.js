var webpack = require("webpack");

// Create a config from the base webpack.config file using our generator
var baseConfig = require("./webpack.config.js").generator();

// webpack-dev-server listens on this port
var port = 9001;

// We need special entry points for hot module replacement to work
baseConfig.entry = [
    "webpack-dev-server/client?http://localhost:" + port,
    "webpack/hot/dev-server"
].concat(baseConfig.entry);

// webpack-dev-server configuration
baseConfig.devServer = {
    contentBase: "./wwwroot/",
    host: "localhost",
    port: port
};

// Include hot module replacement
baseConfig.plugins = baseConfig.plugins.concat(
    new webpack.HotModuleReplacementPlugin()
);

module.exports = baseConfig;