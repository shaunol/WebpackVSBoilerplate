var webpack = require("webpack");

// Create a config from the base webpack.config file using our generator
var baseConfig = require("./webpack.config.js").generator();

// Add production optimization plugins
baseConfig.plugins = baseConfig.plugins.concat(
    new webpack.DefinePlugin({
        "process.env": {
            "NODE_ENV": JSON.stringify("production")
        }
    }),
    new webpack.optimize.DedupePlugin(),
	new webpack.optimize.UglifyJsPlugin()
);

module.exports = baseConfig;