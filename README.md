# Webpack Boilerplate for Visual Studio

## What is it?

This is a basic Visual Studio project that I use as the basis for Javascript-based websites. These websites generally consist of an index.html file and a Javascript entry-point.

This particular project utilizes webpack to pre-process ReactJS (.jsx) and SASS (.scss) files.

## System Requirements
* [Visual Studio 2015 Update 1](https://www.visualstudio.com/en-us/downloads/download-visual-studio-vs.aspx) (Community edition is fine) 
* [node / npm](https://nodejs.org/en/) (I used v5.6 stable)
* [typings](https://github.com/typings/typings) (It's a node module so you'll need npm installed first)
* [NPM Scripts Task Runner](https://visualstudiogallery.msdn.microsoft.com/8f2f2cbc-4da5-43ba-9de2-c9d08ade4941) (optional, recommended)
* [WebPack Task Runner](https://visualstudiogallery.msdn.microsoft.com/5497fd10-b1ba-474c-8991-1438ae47012a) (optional)

## What's Included?
* JSX and SCSS loaders
* webpack-dev-server with hot module replacement
* gulpfile to create production dist package
* npm scripts to control builds

## Getting Started
Open WebpackVSBoilerplate.sln in Visual Studio. For the first time load Visual Studio will automatically restore npm packages. Switch to the Output window to view the progress (You may need to use output selector dropdown at the top of the Output window to show npm output). The Output window can be opened from `View -> Output`.

Eventually npm will complete with: `====npm command completed with exit code 0====`

Switch to the Task Runner Explorer window (View -> Other Windows -> Task Runner Explorer). You will see webpack pre-processing the Javascript and SASS files in the project, as well as launching the webpack-dev-server.

webpack will eventually display the following output: `webpack: bundle is now VALID.` 

At this point webpack-dev-server is now serving the Javascript bundle. Browse to [http://localhost:9001](http://localhost:9001) and you should see the 'Hello, world!' page.

## Debugging
At the moment you still have to use your web browser for debugging, full source maps are there and Chrome provides a nice debugging experience so it's not so bad. However there is work being done for for Visual Studio Code to support breakpoints for Javascript source maps output by webpack. Hopefully it makes it to regular Visual Studio. [See here for the related Github issue](https://github.com/Microsoft/vscode-chrome-debug/issues/40)

Because this project is based on a regular ASP.NET 5 RC1 project template (DNX), the build process is still going to produce output in the artifacts directory. The debugger will also still launch IIS Express or DNX web server. I've left just enough .NET boilerplate code to allow these to serve the index.html and bundle.js files.

The reason for doing this is because this project type has nice npm support which I feel is worth it. I'm still looking into ways to strip out the .NET stuff from the project without causing errors.

## Building
Use the Task Runner Explorer to excute the built-in script `dist`. This will execute a production webpack build.

All output is copied to `artifacts\dist` in the solution root directory by default.

## Configuration Files

### webpack.config.js
webpack.config.js is split into 3 files


| File | Purpose |
| -------- | -------- |
| webpack.config.js      | Base webpack configuration. Defines the Javascript entry point and output as well as a base set of plugins and loaders. Any plugins or loaders you want to add would go here unless they were specific to development or production. |
| webpack.config.dev.js  | Development configuration used when invoking the `watch` and `webpack-dev-server` package.json scripts. Contains configuration for webpack-dev-server and hot module reloading. |
| webpack.config.prod.js | Production configuration used when invoking the `dist` package.json and `dist` gulp scripts. Sets the NODE_ENV to production and includes dedupe and uglify webpack plugins. |


See the [webpack website](https://webpack.github.io/docs/configuration.html#configuration-object-content) for more information on composing a webpack config.


### gulpfile.js
Modify `gulpfile.js` to configure the dist bundle. You may want to add more files to the bundle or change the way webpack is invoked.

You can also add any other custom tasks you may want.

See the [gulp website](https://github.com/gulpjs/gulp) for more information on composing a gulpfile.